var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionSchema = mongoose.Schema({
    question: String,
    questionID: String,
    topicID: String,
    Answers: [{type: Schema.Types.ObjectId, ref: 'Answer'}]
});
// Question model
var Question = mongoose.model('Question', QuestionSchema);

module.exports = Question;