var mongoose = require('mongoose');

//Schema
var TopicSchema = mongoose.Schema({
    topic   : String,
    topicID : String,
    city    : String
});

//Model
Topic = mongoose.model('Topic', TopicSchema);
module.exports = Topic;