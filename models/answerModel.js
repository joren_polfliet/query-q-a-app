var mongoose = require('mongoose');

var AnswerSchema = mongoose.Schema({
    AnswerBody: String,
    questionID: String
});
// Question model
var Answer = mongoose.model('Answer', AnswerSchema);

module.exports = Answer;