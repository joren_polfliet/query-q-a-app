var express = require('express');
var app = express();
var port = process.env.PORT || 3541;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http');
var configDB = require('./config/database.js');
var server = http.createServer(app)

//ROUTES
var questions = require('./routes/answers');
var topics = require('./routes/topic');

//MODELS
var Topic = require('./models/topicModel.js');
var Question = require('./models/questionModel.js');
var Answer = require('./models/answerModel.js');


//MONGODB CONNECTION
mongoose.connect(configDB.url, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log("CONNECTION HAS BEEN ESTABLISHED!");
    }
});


require('./config/passport')(passport);

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

//***************************************
//SOCKETS
//***************************************
var io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket) {


    //***************************************
    //TOPIC SOCKET
    //***************************************    

    Topic.find({}, function (err, docs) {
        if (err) throw err;
        socket.emit('Get Topics', docs);
    });

    //RECEIVE NEW DISCUSSION
    socket.on('create topic', function (data) {

        //SAVE IN MONGODB
        var newTopic = new Topic({
            topic: data.topic,
            topicID: data.topicID,
            city: data.city,
            questions: []
        });
        newTopic.save(function (err) {
            console.log("Saved in DB");
            if (err) throw err;

            //SEND DISCUSSION TO CLIENTS
            io.sockets.emit('Get New Topics', data);
        });
    });
    

    //***************************************
    //QUESTION SOCKET
    //***************************************
    
    //GET QUESTIONS FROM MONGODB
    Question.find({}).populate('Answers').exec(function (err, docsQuestions) {
        if (err) throw err;
        socket.emit('Get Questions', docsQuestions);
    });

    //RECEIVE NEW QUESTION
    socket.on('Send Question', function (data) {

        //SAVE IN MONGODB
        var newQuestion = new Question({
            question: data.question,
            questionID: data.questionID,
            topicID: data.topicID,
            Answers: []
        });
        newQuestion.save(function (err, qst) {
            if (err) throw err;

            //SEND QUESTION TO CLIENTS
            io.sockets.emit('Get New Questions', qst);
        });
    });

    
    //***************************************
    //ANSWER SOCKET
    //***************************************
    
    //RECEIVE NEW ANSWER
    socket.on('Send Answer', function (data) {
        Question.findOne({
            _id: data.qstid
        
        //SAVE IN MONGODB
        }, function (err, questiondata) {
            var newAnswer = new Answer({
                AnswerBody: data.AnswerBody,
                questionID: data.questionID
            });
            
            newAnswer.save(function (err, AnswerBody) {
                if (err) throw err;
                if (questiondata.Answers == null) {
                    questiondata.Answers = [];
                }
                questiondata.Answers.push(AnswerBody);
                questiondata.save(function (e) {
                    
                    //SEND QUESTION TO CLIENTS
                    io.sockets.emit('Get New Answers', data);
                });
            });
        });
    });

    
    //***************************************
    //USERCOUNT SOCKET
    //***************************************
    
    var userCount = 0;
    
    //CHECK IF USER CONNECTS
    io.sockets.on('connection', function (socket) 
    {
        userCount++;
        
        //UPDATE USERCOUNT
        io.sockets.emit('userCount', { userCount: userCount });
        
        //CHECK IF USER DISCONNECTS
        socket.on('disconnect', function() {
            userCount--;
            
            //UPDATE USERCOUNT
            io.sockets.emit('userCount', { userCount: userCount });
        });
    });


    //***************************************
    //ANSWER SOCKET
    //***************************************
    /* Answer.find({}, function (err, docsAnswers) {
         if (err) throw err;
         socket.emit('Get Answers', docsAnswers);
     });


     socket.on('Send Answer', function (data) {

         var newAnswer = new Answer({
             AnswerID: data.answerID,
             AnswerBody: data.AnswerBody,
             questionID: data.questionID
         });
         newAnswer.save(function (err, answ) {
             if (err) throw err;

             io.sockets.emit('Get New Answers', answ);
         });
     });*/


});

app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs');

app.use('/topic', topics);
app.use('/question', questions);

app.use(session({
    secret: 'webtech',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./routes/login')(app, passport);

server.listen(port);
console.log('Server is running on port - ' + port);
