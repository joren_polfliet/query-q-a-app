$(document).ready(function () {
    var socket = io.connect();
    var $questForm = $('#send-quest');
    var $questBox = $('#quest');
    var $quests = $('#quests')
    var $topicID = $('#topicID');
    var userCount = 0;

    var q = 1;

    $questForm.submit(function (e) {
        e.preventDefault();
        console.log("submitted from questionform");
        
        //SEND QUESTION
        socket.emit('Send Question', {
            question: $questBox.val(),
            questionID: q,
            topicID: $topicID.val()
        });
        $questBox.val('');
    });

    //RECEIVE OLD QUESTIONS
    socket.on('Get Questions', function (docsQuestions) {
        console.log(docsQuestions);
        for (var i = 0; i < docsQuestions.length; i++) {
            if ($topicID.val() == docsQuestions[i].topicID) {
                displayQuest(docsQuestions[i]);
            }
        }
    });


    //RECEIVE NEW QUESTIONS
    socket.on('Get New Questions', function (data) {
        if (data.topicID == $topicID.val()) {
            displayQuest(data);
            $("#quests div").addClass("fadeInTop");
        }
    });
    
    //DISPLAY USER COUNT
    socket.on('userCount', function (data) {
        console.log('lol');
        var $UserCounter = $('#UserCount');
        if (data.userCount == 1) {
            $UserCounter.html(data.userCount + " user participating");
        } else {
            $UserCounter.html(data.userCount + " users participating");
        }

    });

    function displayQuest(data) {
        q++;
        $quests.prepend("<div class='questiondiv hidden' id='question" + data.questionID + "'><p class='quest'>" + data.question + "</p><div class='answerdiv'><form class='answerForma' id='AnswerForm" + data.questionID + "'><input type='text' class='answerInput' id='answerQ" + data.questionID + "'><input type='hidden' id='questionID" + data.questionID + "' value='" + data.questionID + "'> <input type='hidden' id='qID" + data.questionID + "' value='" + data._id + "'><input type='submit' id='answerBtn' value='Antwoord'></form><div class='scollClass' id='AnswerDiv" + data.questionID + "'></div></div>");
        $('.questiondiv').fadeIn(1000);

        var $formid = "#AnswerForm" + data.questionID;
        var $answerForm = $($formid);
        var $answerBox = $('#answerQ' + data.questionID);
        var $answers = $('#AnswerDiv' + data.questionID);
        var $questionID = $('#questionID' + data.questionID);
        var $qstid = $('#qID' + data.questionID);

        if (data.Answers != null && data.Answers.length > 0) {
            for (var i = 0; i < data.Answers.length; i++) {
                displayAnswer(data.Answers[i]);
            }
        }

        $answerForm.submit(function (e) {
            e.preventDefault();
            socket.emit('Send Answer', {
                AnswerBody: $answerBox.val(),
                questionID: $questionID.val(),
                formid: $formid,
                qstid: $qstid.val()
            });
            $answerBox.val('');
        });
    }


    function displayAnswer(docsAnswers) {
        var $answers = $('#AnswerDiv' + docsAnswers.questionID);
        var answerString = docsAnswers.AnswerBody.substring(0, 8);
        var answerStringHttp = docsAnswers.AnswerBody.substring(0, 7);

        if (answerString == "https://") {
            var answerImageTag = "<img class='imageAnswer' src='" + docsAnswers.AnswerBody + "' alt='ImageAnswer' />";
            $answers.prepend("<div class='answerdiv hidden' id='answer'>" + answerImageTag + "</div>");
        } else if (answerStringHttp == "http://") {
            var answerImageTag = "<img class='imageAnswer' src='" + docsAnswers.AnswerBody + "' alt='ImageAnswer' />";
            $answers.prepend("<div class='answerdiv hidden ' id='answer'>" + answerImageTag + "</div>");
        } else {
            $answers.prepend("<div class='answerdiv hidden' id='answer'><p class='answer'>" + docsAnswers.AnswerBody + "</p></div>");
        }
        $('.answerdiv').fadeIn(1000);

    };
    // receiving new reply from server
    socket.on('Get New Answers', function (data) {
        displayAnswer(data);
    });
});
