var locationTitle = $('#locationTitle');

$('#locationTopicsLink').click(function()
{
    $('#allTopicsDiv').fadeOut(200);
    locationTitle.html("<p>Discussions In "+ location +"</p>");
    $('#locationTopicsDiv').fadeIn(1000);
});

$('#allTopicsLink').click(function(){
    $('#locationTopicsDiv').fadeOut(200);
    locationTitle.html("<p>All Discussions</p>");
    $('#allTopicsDiv').fadeIn(1000);
});
