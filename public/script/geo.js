$(document).ready(function () {
	var locationTitle = $('#locationTitle');
    var locationField = $('#topic_location');
	if (navigator.geolocation) 
    {
		navigator.geolocation.getCurrentPosition(getLocation);
	}else{
		locationDiv.innerHTML = "Niet Ondersteund";
	}

	function getLocation(position){
		return Location(position.coords.latitude, position.coords.longitude);
	}

	var Location = function (lat, lon){
			this.latitude = lat;
			this.longitude = lon;
			getData();
	};

	var getData = function(){
		var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+this.latitude+","+this.longitude+"&sensor=true";

  		$.ajax({
      		type: "GET",
      		url: url,
      		dataType: "json", 
        
        success: function(response) {
            getLocationName(response);
          },

        error: function() {
          console.log("Error");
        }});
	};

	getLocationName = function(arg){
  		var location = arg.results[0].address_components[2].short_name;
  		
  		locationTitle.html("<p>Discussions in "+ location +"</p>");
        locationField.val(location);
	};
});