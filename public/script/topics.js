$(document).ready(function () {
    var socket = io.connect();
    var $topicForm = $('#create_topic_form');
    var $topicName = $('#topic_name');
    var $topicLocation = $('#topic_location');
    var $locationTopicsDiv = $('#locationTopics');
    var $allTopicsDiv = $('#allTopics');
    var first_time_load = 1;

    var i = 1;

    $topicForm.submit(function (e) {
        e.preventDefault();

        //SEND TOPIC
        socket.emit('create topic', {
            topic: $topicName.val(), 
            topicID: i,
            city: $topicLocation.val()
        });
        $topicName.val('');
    });

    //RECEIVE OLD TOPICS
    socket.on('Get Topics', function (docs) {
        for (var i = 0; i < docs.length; i++) {
            displayTpcFirstTime(docs[i]);
        }
    });


    //RECEIVE NEW TOPICS
    socket.on('Get New Topics', function (data) {
        displayTpc(data);
    });

    function displayTpcFirstTime(data) {
        setTimeout(function() 
        {
            if ($topicLocation.val() == data.city)
            {
                $locationTopicsDiv.prepend("<a href='/topic/id/" + data.topicID + "'><div class='listItem' id='TopicNew" + i + "'><p>" + data.topic + "</p></div></a>");
                
                $allTopicsDiv.prepend("<a href='/topic/id/" + data.topicID + "'><div class='listItem' id='TopicNew" + i + "'><p>" + data.topic + "</p></div></a>");
                i++;    
            }else{
                $allTopicsDiv.prepend("<a href='/topic/id/" + data.topicID + "'><div class='listItem' id='TopicNew" + i + "'><p>" + data.topic + "</p></div></a>");
                i++;
            }
        }, 500);
    }
    
    function displayTpc(data) {
        if ($topicLocation.val() == data.city)
        {
            $locationTopicsDiv.prepend("<a class='hidden fadenewitem' href='/topic/id/" + data.topicID + "'><div class='listItem' id='TopicNew" + i + "'><p>" + data.topic + "</p></div></a>");
            
            $allTopicsDiv.prepend("<a class='hidden fadenewitem' href='/topic/id/" + data.topicID + "'><div class='listItem' id='TopicNew" + i + "'><p>" + data.topic + "</p></div></a>");
            $('.fadenewitem').fadeIn(1000);
            i++;    
        }else{
            $allTopicsDiv.prepend("<a class='hidden fadenewitem' class='hidden fadenewitem' href='/topic/id/" + data.topicID + "'><div class='listItem' id='TopicNew" + i + "'><p>" + data.topic + "</p></div></a>");
            
            $('.fadenewitem').fadeIn(1000);
            i++;
        }
    }
});
