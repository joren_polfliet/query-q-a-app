$(document).ready(function () {
    var socket = io.connect();
    var $answerForm = $('#send-answer');
    var $answerBox = $('#answer');
    var $answers = $('#answerDiv')
    var $questionID = $('#questionID');;

    var a = 1;

    //send answer to the server
    $answerForm.submit(function (e) {
        e.preventDefault();
        console.log("saving answer");
        socket.emit('Send Answer', {
            AnswerBody: $answerBox.val(),
            questionID: $questionID.val()
        });
        $answerBox.val('');
    });

    //receiving old answers from server
    socket.on('Get Answers', function (docsAnswers) {

        for (var i = 0; i < docsAnswers.length; i++) {
            if ($questionID.val() == docsAnswers[i].questionID) {
                displayAnswer(docsAnswers[i]);
            }
        }
    });


    // receiving new answers from server
    socket.on('Get New Answers', function (data) {
        if (data.questionID == $questionID.val()) {
            displayAnswer(data);
            $("#answerDiv").addClass("fadeInTop");
        }
    });

    function displayAnswer(data) {
        var answerString = data.AnswerBody.substring(0, 8);
        var answerStringHttp = data.AnswerBody.substring(0, 7);
        a++;

        if (answerString == "https://") {
            var answerImageTag = "<img class='imageAnswer' src='" + data.AnswerBody + "' alt='ImageAnswer' />";
            $answers.prepend("<div class='answerText' id='answerText'>" + answerImageTag + "</div>")
        } else if (answerStringHttp == "http://") {
            var answerImageTag = "<img class='imageAnswer' src='" + data.AnswerBody + "' alt='ImageAnswer' />";
            $answers.prepend("<div class='answerText' id='answerText'>" + answerImageTag + "</div>")
        } else {
            $answers.prepend("<div class='answerText' id='answerText'><p class='answer'>" + data.AnswerBody + "</p></div>")
        }


    }
});