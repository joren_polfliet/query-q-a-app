var express = require('express');
var router = express.Router();
var Question = require('../models/questionModel.js');

router.get('/id/:id', function(req, res, next) {
   
    var id = req.params.id;
    Question.findOne({questionID: id}, function(err, docsQuestion){
        res.render('questionDetail.ejs', {docsQuestions: docsQuestion});
    });
    
});

module.exports = router;