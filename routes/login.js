module.exports = function (app, passport) {
    app.get('/', function (req, res) {
        res.render('login.ejs', {
            message: req.flash('loginMessage')
        });
    });

    app.get('/register', function (req, res) {
        res.render('index.ejs', {
            message: req.flash('signupMessage')
        });
    });

    app.post('/', passport.authenticate('local-login', {
        successRedirect: '/topic',
        failureRedirect: '/',
        failureFlash: true
    }));

    app.post('/register', passport.authenticate('local-signup', {
        successRedirect: '/topic',
        failureRedirect: '/register',
        failureFlash: true
    }));
    

    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: 'email'
    }));

    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/topic',
            failureRedirect: '/'
        }));

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}
