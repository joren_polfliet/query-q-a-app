var express = require('express');
var router = express.Router();
var Topic = require('../models/topicModel.js');

router.get('/id/:id', function(req, res, next) {
   
    var id = req.params.id;
    Topic.findOne({topicID: id}, function(err, doc){
        res.render('questions.ejs', {docs: doc});
    });
    
});

module.exports = router;