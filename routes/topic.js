var express = require('express');
var router = express.Router();
var Topic = require('../models/topicModel.js');

    router.get('/', function(req, res, next) {
        res.render('create_topic.ejs', { title: 'Topics', user: req.user });
    });
    
    router.get('/id/:id', function(req, res, next) {
        var id = req.params.id;

        Topic.findOne({topicID: id}, function(err, doc){
            res.render('questions.ejs', {docs: doc});
        });
    });

module.exports = router;