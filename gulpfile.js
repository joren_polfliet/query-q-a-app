var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var concatCss = require('gulp-concat-css');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
//var coffee = require('gulp-coffee');
var uglify = require('gulp-uglify');
 
 

gulp.task('default', function() {
    
});

gulp.task('minify-css', function() {
    return gulp.src('public/css/*.css')
        .pipe(concatCss("css/bundle.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('public/build/css'));
});

gulp.task('imagemin', function() {
    return gulp.src('public/img/*')
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest('public/build/images'));
});
 
gulp.task('sass', function () {
    return gulp.src('public/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/build/sass'));
});
 
gulp.task('sass:watch', function () {
    gulp.watch('public/sass/**/*.scss', ['sass']);
});
gulp.task('scripts', function(){
	gulp.src('public/script/*.js')
	.pipe(plumber())
	.pipe(uglify())
	.pipe(gulp.dest('public/build/javascripts'));
});
